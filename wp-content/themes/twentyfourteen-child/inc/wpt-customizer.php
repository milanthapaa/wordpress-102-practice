<?php 

function wpt_register_theme_customizer($wp_customize) {
	// echo "<pre>";
	// var_dump($wp_customize->sections());
	// echo "</pre>";

	// Customize title and tagline sections and labels
	$wp_customize->get_section('title_tagline')->title = __('Site Title, Tag Line and Favicon', 'wptthemecustomizer');
	$wp_customize->get_control('blogname')->label = __('Site Name', 'wptthemecustomizer');
	$wp_customize->get_control('blogdescription')->label = __('Site Description', 'wptthemecustomizer');

	// Customize the Front Page Settings
	$wp_customize->get_section('static_front_page')->title = __('Homepage Preferences', 'wptthemecustomizer');
	$wp_customize->get_section('static_front_page')->priority = 20;
	$wp_customize->get_control('show_on_front')->label = __('Choose Homepage Preference', 'wptthemecustomizer');
	$wp_customize->get_control('page_on_front')->label = __('Select Homepage', 'wptthemecustomizer');
	$wp_customize->get_control('page_for_posts')->label = __('Select Blog Homepage', 'wptthemecustomizer');



	// Add Custom Footer Text

	$wp_customize->add_panel( 'custom_footer_panel',
		array(
			'title' => __( 'Custom Footer Text' ),
			'description' => esc_html__( 'Adjust your Footer Text.' ) 
      		// Include html tags such as 
		)
	);

	$wp_customize->add_section('custom_footer_text', array(
		'title'		=> __('Change Footer Text', 'wptthemecustomizer'),
		'panel'		=> 'custom_footer_panel', 
		'priority'	=> 1000

	));

	$wp_customize->add_setting(
		'wpt_footer_text',
		// wpt_footer_text is use in jquery purpose for live preview
		array(
			'default'	=> __(	'Custom footer text', 'wptthemecustomizer'),
			'transport'	=> 'postMessage', 
			'sanitize_callback' => 'sanitize_text'
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'custom_footer_text',
			array(
				'label' 	=> __('Footer Text', 'wptthemecustomizer'),
				'section'	=> 'custom_footer_text',
				'settings'	=> 'wpt_footer_text',
				'type'		=> 'text'
			)
		)
	);


}

add_action('customize_register', 'wpt_register_theme_customizer');



?>