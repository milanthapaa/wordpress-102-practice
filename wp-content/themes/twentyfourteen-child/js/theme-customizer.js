(function($){
	wp.customize('wpt_footer_text', function(value){
		value.bind(function(to){
			if(to == '') {
				$('#footertext').hide();
			} else {
				$('#footertext').show();
				$('#footertext').text(to);
			}
		})
	})
})(jQuery);