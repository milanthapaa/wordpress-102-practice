<?php
/**
 * Enqueuing Css From Parent Theme
 */

add_action( 'wp_enqueue_scripts', 'twentyfourteen_child_theme_enqueue_styles' );
function twentyfourteen_child_theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_script('customizer', get_stylesheet_directory_uri() . '/js/theme-customizer.js', array(), '', true);

}


/**
 * Enqueuing Admin Color Scheme
 * Just Replace The Color In The Css File After Copying Css File From 
 * wp-admin->css->colors->ectoplasm->colors.css To 
 * css->admin-colors->Treehouse->colors.css
 */

function twentyfourteen_child_admin_color_schemes() {
	$theme_dir = get_stylesheet_directory_uri();

	wp_admin_css_color( 'treehouse', __('Treehouse'), $theme_dir . '/css/admin-colors/treehouse/colors.css', array('#384047', '#5BC67B', '#838cc7', '#ffffff') 
);
}

add_action('admin_init', 'twentyfourteen_child_admin_color_schemes');


/**
 * Enqueuing Login Css
*/

function twentyfourteen_child_login_stylesheet() {
	wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/css/login/login.css' );
}
add_action( 'login_enqueue_scripts', 'twentyfourteen_child_login_stylesheet' );


/**
 * Change The Login Logo URL
*/

function twentyfourteen_child_login_logo_url() {
	return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'twentyfourteen_child_login_logo_url' );

function twentyfourteen_child_logo_url_title() {
	return 'Default Site Title';
}
add_filter( 'login_headertitle', 'twentyfourteen_child_logo_url_title' );


/**
 * Hide The Login Error Message
*/

function twentyfourteen_child_login_error_message()
{
	return 'Please enter valid login credentials.';
}
add_filter('login_errors', 'twentyfourteen_child_login_error_message');


/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function twentyfourteen_child_add_dashboard_widgets() {

	wp_add_dashboard_widget(
                 'twentyfourteen_child_welcome_dashboard_widget',         // Widget slug.
                 'Welcome To Your Site!',         // Title.
                 'twentyfourteen_child_dashboard_widget_function' // Display function.
             );	
}
add_action( 'wp_dashboard_setup', 'twentyfourteen_child_add_dashboard_widgets' );


/**
 * Create the function to output the contents of our Dashboard Widget.
 */

function twentyfourteen_child_dashboard_widget_function() {

	// Display whatever it is you want to show.
	echo "<p>If you need help, please contact milanthapa299@gmail.com</p>";
	echo "<p>For instructions on editing your site, check you the <a>instruction manual.</a>";
}

/**
 * Remove Menu From Admin Area
*/

function twentyfourteen_child_footer_admin () {
	echo "<p>Thank you for the opportunity. If you ever need an assistance please inbox me <a href='https://www.linkedin.com/in/thapamilan/' target='_blank'>@milanthapa.</a></p>";
}
add_filter('admin_footer_text', 'twentyfourteen_child_footer_admin');

/**
 * Remove Menu From Admin Area
*/

// function remove_menus() {
// 	remove_menu_page( 'index.php' );                  //Dashboard
// 	remove_menu_page( 'jetpack' );                    //Jetpack* 
// 	remove_menu_page( 'edit.php' );                   //Posts
// 	remove_menu_page( 'upload.php' );                 //Media
// 	remove_menu_page( 'edit.php?post_type=page' );    //Pages
// 	remove_menu_page( 'edit-comments.php' );          //Comments
// 	remove_menu_page( 'themes.php' );                 //Appearance
// 	remove_menu_page( 'plugins.php' );                //Plugins
// 	remove_menu_page( 'users.php' );                  //Users
// 	remove_menu_page( 'tools.php' );                  //Tools
// 	remove_menu_page( 'options-general.php' );        //Settings
// }
// add_action( 'admin_menu', 'remove_menus' );


/**
 * Remove Dashboard Widget
*/

// function remove_dashboard_meta() {
//         remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
//         remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
//         remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
//         remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
//         remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
//         remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
//         remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
//         remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
//         remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');//since 3.8
// }
// add_action( 'admin_init', 'remove_dashboard_meta' );


/**
 * Loading Customizer File
*/

require_once( get_stylesheet_directory(). '/inc/wpt-customizer.php' );



/**
 * Adding Custom Logo P.S In Current Version of Wordpress One Logo Field Comes By Default
*/

add_theme_support( 'custom-logo' );


/**
 * Sanitize Text
 */

function sanitize_text($text) {
	return sanitize_text_field( $text );
}


/**
 * Enqueuing Js For Theme Customizer
 */

add_action( 'wp_enqueue_scripts', 'twentyfourteen_child_theme_customizer_enqueue_script' );
function twentyfourteen_child_theme_customizer_enqueue_script() {
	wp_enqueue_script('customizer', get_stylesheet_directory_uri() . '/js/theme-customizer.js', array(), '', true);

}

?>