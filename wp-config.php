<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'customizer_api' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '$<WHW#zF>5<4->B:TNf2?0A+|vx7TI>E`sz~}6jLZGhSY(QmIv~uT7r@65C5y,,i' );
define( 'SECURE_AUTH_KEY',  '8<@jZl1v;/a~:1)?d9f;?X{B}~>i:jp#hh[?c*X9#?-Od:}DW*U5do:Oa8HB0Cr<' );
define( 'LOGGED_IN_KEY',    'h_w~e[NS=^t_#9u)$l/}&WCT%Z4<iHz4b_$E%rA[oJ^J`8a^AsqHE!k7m.2N38Yh' );
define( 'NONCE_KEY',        '*pP*xxR|Di{Kb^Z<IP8.VlpK4Bv v:B]@[wta)|$>AxC=pOkA+~:JFpcQ{qKZ`(Y' );
define( 'AUTH_SALT',        'yP3;bbpn@hWqFK!owWX@GNzdq]N]_uV%5{9C5hAUH+ 2@u&|K5pPL3vr!@!4ZNbT' );
define( 'SECURE_AUTH_SALT', 'D^- khz|x2y+4BU<C)#!AvK{DCU:cLSQh[D.rmk-rr7[T-<U90@<N])2Fa_:QII*' );
define( 'LOGGED_IN_SALT',   'wa!V!kQ-4XesW2{fN!`C]&rim{hSAfAqoS,v6#d4?{Y*=olhnwvxtTyBHcBtF+Fj' );
define( 'NONCE_SALT',       'XXe<Djw>qwwM|s(Q5a<2&M*o4C^//QMq&&@|;~Py5@LD`h~nh2WQUK]1?mmDdfAn' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
